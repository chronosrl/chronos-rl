//
//  iosConnectivity.swift
//  ChronosRL
//
//  Created by Felipe Lukavei Ferreira on 4/17/16.
//  Copyright © 2016 Felipe Lukavei Ferreira. All rights reserved.
//

import UIKit
import WatchConnectivity

class iosConnectivity: NSObject, WCSessionDelegate {
    var session : WCSession?{
        didSet{
            if let session = session{
                session.delegate = self
                session.activateSession()
            }
        }
    }

    func transferInfo (dataDictionary: Dictionary<String, AnyObject>)
    {
        session?.transferUserInfo(dataDictionary)
        session?.transferCurrentComplicationUserInfo(dataDictionary)
    }
}
