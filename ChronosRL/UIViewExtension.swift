//
//  UIViewExtension.swift
//  ChronosRL
//
//  Created by Leonardo Piovezan on 4/17/16.
//  Copyright © 2016 Felipe Lukavei Ferreira. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    private struct BlockProperties {
        static var block:Block?
    }
    
    var block:Block? {
        get{
            
            return objc_getAssociatedObject(self, &BlockProperties.block) as? Block
               // BlockProperties.block
        }
        set(newValue)
        {
            if let unwrappedValue = newValue {
                objc_setAssociatedObject(self, &BlockProperties.block, unwrappedValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            }
            
            BlockProperties.block = newValue
        }
    }
    
}
