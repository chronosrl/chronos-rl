//
//  TimelineScrollView.swift
//  ChronosRL
//
//  Created by Felipe Ramon de Lara on 4/11/16.
//  Copyright © 2016 Felipe Lukavei Ferreira. All rights reserved.
//

import UIKit

class TimelineScrollView: UIScrollView {

    var blockViewArray = Array<UIView>()
    var touchedBlock: Bool? = false
    var selectedBlock = UIView()
    var intersection:CGRect?
    private var swipe = false
    private var configureBlockInfo:(UIView)->()?
    
  
    init(frame: CGRect, function:(UIView)->()) {
        configureBlockInfo = function
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
    
    func darken(){
        UIView.animateWithDuration(0.2, delay: 0, options: .TransitionCurlUp, animations: {
            self.alpha = 0.5
            }, completion: { finished in
        })

    }
    func lighten(){
        UIView.animateWithDuration(0.2, delay: 0, options: .TransitionCurlUp, animations: {
            self.alpha = 1
            }, completion: { finished in
        })
    }
    
    override func touchesShouldBegin(touches: Set<UITouch>, withEvent event: UIEvent?, inContentView view: UIView) -> Bool {
        return true
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {

        for touch in touches {
            let position = touch.locationInView(self)
            
            for block in blockViewArray{
                if (CGRectContainsPoint(block.frame, position) ){
                    touchedBlock = true
                    selectedBlock = block
                    
                    UIView.animateWithDuration(0.4, delay: 0, options: .TransitionCurlUp, animations: {
                        block.transform = CGAffineTransformMakeScale(1.3, 1.3)
                        }, completion: { finished in
                            //print("animation no begaaaan")
                        
                    })
                    
                    break
                }else{
                    touchedBlock = false
                }
            }
        }
    }
    
    func sortBlockArray()
    {
        blockViewArray.sortInPlace({$0.frame.origin.x < $1.frame.origin.x})
        
//        for block in blockViewArray {
//            
//          //  print("A posição x: \(block.frame.origin.x)")
//        }
    }
    
    func adjustBlockGaps(){
        var countWidth = CGFloat()
        countWidth = 0
        if blockViewArray.count > 0{
            blockViewArray[0].frame.origin.x = 0
        }
        
        for (index, block) in blockViewArray.enumerate(){
            block.frame.origin.y = 0
            countWidth += block.frame.width
            if index + 1 < blockViewArray.count{
                blockViewArray[index + 1].frame.origin.x = countWidth
                //                if block.frame.origin.x + block.frame.width /* + 10*/ < blockViewArray[index+1].frame.origin.x{
                //                    blockViewArray[index + 1].frame.origin.x = block.frame.origin.x + block.frame.width //+ 10
                //                }
            }
            
        }//commit inutil
    }
    
    func moveBlocksByItsRight(incomingBlock: UIView, convertedRect: CGRect){
        
        
        for block in blockViewArray{
//            print("X do block que intersecta: \(block.frame.origin.x)")
//            print("X da intersecção: \(CGRectIntersection(block.frame, convertedRect).origin.x)")
            let frameIntersection = CGRectIntersection(block.frame, convertedRect)

            if !frameIntersection.isEmpty && frameIntersection.origin.x > block.frame.origin.x{
                incomingBlock.frame.origin.x += frameIntersection.width //+ 10
                moveBlocksByItsRight(incomingBlock, convertedRect: incomingBlock.frame)

                break
               // moveBlocksByItsRight(incomingBlock, convertedRect: incomingBlock.frame)
            }else if !frameIntersection.isEmpty && frameIntersection.origin.x == block.frame.origin.x && incomingBlock != block /*block.frame != frameIntersection*/ {
                
                block.frame.origin.x += frameIntersection.width //+ 10
                moveBlocksByItsRight(block, convertedRect: block.frame)
                continue
            }
            
            
//            if !frameIntersection.isEmpty && frameIntersection.origin.x < block.frame.origin.x + block.frame.width && convertedRect != block.frame{
//                print("o valor da inersecçao é : \(frameIntersection)")
//                incomingBlock.frame.origin.x += frameIntersection.width + 10
//                
//                print("posicao do incoming block: \(incomingBlock.frame)")
//              
//                //selectedBlock.frame.origin.x += frameIntersection.width + 10
//               // block.frame.origin.x += frameIntersection.width + 10
//                
//               // moveBlocksByItsRight(block.frame)
//            }
//            if !frameIntersection.isEmpty && frameIntersection.origin.x == block.frame.origin.x && convertedRect != block.frame{
//                
//               // block.frame.origin.x += frameIntersection.width + 10
//                
//                moveBlocksByItsRight(incomingBlock, convertedRect: CGRect())
//            }
        }
    }
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        for touch in touches {
            
            if(touchedBlock!){
                swipe = true
                self.canCancelContentTouches = false
                selectedBlock.center = CGPointMake(touch.locationInView(self).x, touch.locationInView(self).y);
                selectedBlock.alpha = 0.5
                //print("movedddddd")
              
            }
        }
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        UIView.animateWithDuration(0.3, delay: 0, options: .TransitionCurlUp, animations: {
            self.selectedBlock.transform = CGAffineTransformMakeScale(1, 1)
            self.selectedBlock.alpha = 1.0
            if self.swipe{
                print("moveu bloco")
            } else
            {
                print("NOME DO BLOCO QUE TEMOS AQUI: \(self.selectedBlock.block?.getLabel())")
                print(self.selectedBlock.block?.getSoundStatus())
                print(self.selectedBlock.block?.getVibrationStatus())
                print("a duracao do bloco é: \(self.selectedBlock.block?.getDurationInSeconds())")
                //descobrir qual é o bloco e carregar suas informações
                if (self.touchedBlock == true){
                self.configureBlockInfo(self.selectedBlock)
                print("fez tap")
                }
         
            }
            if CGRectIntersection(self.bounds, self.selectedBlock.frame) != self.selectedBlock.frame
            {
                self.intersection = CGRectIntersection(self.superview!.frame, self.selectedBlock.frame)
                
                print(self.checkIfBlockIntersects())
            
            }
            }, completion: { finished in
                self.swipe = false
                if let _ = self.intersection{
                    
//                    self.selectedBlock.removeFromSuperview()
//                    self.selectedBlock.center = (self.superview?.center)!
//                   // self.selectedBlock.frame = self.convertScreenCoordinateToScrollView(self.intersection!)
//                    self.superview!.addSubview(self.selectedBlock)
//                    self.intersection = nil
                    

                }else{
                    self.moveBlocksByItsRight(self.selectedBlock, convertedRect: self.selectedBlock.frame)
                
                }
        })
        self.sortBlockArray()
        self.adjustBlockGaps()
        touchedBlock = false
    }


    func checkIfBlockIntersects()->Bool{
        
        for block in blockViewArray{
            if CGRectIntersectsRect(selectedBlock.frame, block.frame) && block != selectedBlock
            {
                return true
            }
        
    }
            return false
    }

    func getFuncFromSuperview(function:(UIView)->())
    {
        function(selectedBlock)
    }
    
    func removeBlockFromArray(blockToRemove:UIView)
    {
        for (index,block) in blockViewArray.enumerate()
        {
            if block.isEqual(blockToRemove)
            {
                blockViewArray.removeAtIndex(index)
                return
            }
            
        }
    }
    
}