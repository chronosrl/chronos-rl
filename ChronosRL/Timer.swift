//
//  Timer.swift
//  ChronosRL
//
//  Created by Leonardo Piovezan on 4/15/16.
//  Copyright © 2016 Felipe Lukavei Ferreira. All rights reserved.
//

import UIKit

class Timer: AnyObject {
    
    private var timer:NSTimer?
    private var currentTime = 0
    private var totalTime = 0
    private var secondsTime = 0
    private var minutesTime = 0
    private var hoursTime = 0
    private var updateFunction:()->()?
    
    
    init(time:Int, functionInUpdate:()->())
    {
        totalTime = time
        updateFunction = functionInUpdate
        
    }
    
    func startTimer()
    {
       timer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: #selector(Timer.update), userInfo: nil, repeats: true)
    }
    
    func pauseTimer()
    {
        timer?.invalidate()
    }
    
    func stopTimer()
    {
        timer?.invalidate()
        currentTime = 0
        secondsTime = 0
        minutesTime = 0
        hoursTime = 0
        updateFunction()
    }
    
    @objc private func update()
    {
        convertTime()
        if currentTime == totalTime
        {
            stopTimer()
        }
        print(currentTime)
        currentTime += 1
        updateFunction()
        
    }
    
    func getTime()->Int
    {
        return currentTime
    }
    
    private func convertTime()
    {
        hoursTime = currentTime/3600
        minutesTime = (currentTime - (hoursTime)*3600)/60
        secondsTime = (currentTime - (hoursTime)*3600 - minutesTime*60)
    }
    
    func getSecondsString()->String{
        return intToStringFormat(secondsTime)
    }
    
    func getMinutesString()-> String {
        return intToStringFormat(minutesTime)
    }
    
    func getHoursString() -> String {
        
        return intToStringFormat(hoursTime)
    }
    
    private func intToStringFormat(number:Int)->String {
        return String(format: "%02d", number)
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}