//
//  FileManager.swift
//  ChronosRL
//
//  Created by Eduardo Brandalise on 22/04/16.
//  Copyright © 2016 Felipe Lukavei Ferreira. All rights reserved.
//

import UIKit
import SwiftyJSON

class FileManager {
    
    static let sharedInstance = FileManager()
    
    private let presentationFileName = "Presentations"
    private let presentationFileType = "json"
    
    private func readFile(fileName: String, fileType: String) -> JSON? {
        
        // Busca o arquivo no Documents.
        if let filePath = getFilePath(fileName, fileType: fileType) {
            
            if let data = NSData(contentsOfFile: filePath) {
                let json = JSON(data: data)
                return json
            }
                
                // Busca o arquivo no Bundle caso não encontre no Documents.
            else {
                if let bundlePath = getBundlePathForResource(fileName, fileType: fileType) {
                    
                    do {
                        let data = try NSData(contentsOfFile: bundlePath, options: .DataReadingMappedIfSafe)
                        
                        let json = JSON(data: data)
                        return json
                        
                    } catch let error as NSError {
                        print(error.description)
                    }
                }
            }
        }
        
        return nil
    }
    
    private func writePresentationJSON(fileName: String, fileType: String, presentationArray: Array<Presentation>) {
        
        let json = createJSONFromPresentationArray(presentationArray)
        
        if let documentsPath = getDocumentsPath() {
            let filePath = documentsPath + "/\(fileName).\(fileType)"
            let string = json.description
            let data = string.dataUsingEncoding(NSUTF8StringEncoding)!
            
            do {
                try data.writeToFile(filePath, options: .AtomicWrite)
            } catch let error as NSError {
                print(error.description)
            }
        }
    }
    
    // Writes a new presentation in json.
    func saveNewPresentation(newPresentation: Presentation) {

        var presentations = getPresentationsFromFile(presentationFileName, fileType: presentationFileType)
        
        presentations.append(newPresentation)
        writePresentationJSON(presentationFileName, fileType: presentationFileType, presentationArray: presentations)
    }
    
    // Removes a presentation from JSON file at given index.
    func deletePresentation(index: Int) {
        var presentations = getPresentationsFromFile(presentationFileName, fileType: presentationFileType)
        
        presentations.removeAtIndex(index)
        self.writePresentationJSON(presentationFileName, fileType: presentationFileType, presentationArray: presentations)
    }
    
    func moveFileFromBundleToDocuments(fileName: String, fileType: String) {

        let fileBundlePath = getBundlePathForResource(fileName, fileType: fileType)
        let documentsPath = getDocumentsPath()
        let newFilePath = documentsPath! + "/\(fileName).\(fileType)"
        let fileManager = NSFileManager.defaultManager()
        
        if !checkIfFileExistsInDocuments(fileName, fileType: fileType) {
            do {
                try fileManager.copyItemAtPath((fileBundlePath)!, toPath: newFilePath)
            } catch let error as NSError {
                print(error.description)
            }
        }
    }
    
    private func checkIfFileExistsInDocuments (fileName: String, fileType: String) -> Bool {
        var itExists = false
        let fileManager = NSFileManager.defaultManager()
        let filePathInDocuments = getFilePath(fileName, fileType: fileType)
        
        
        if fileManager.fileExistsAtPath(filePathInDocuments!) {
            itExists = true
            return itExists
            
        } else {
            
            return itExists
            
        }
    }
    
    private func createJSONFromPresentationArray (presentationArray: Array<Presentation>) -> JSON {
        var jsonArray = Array<JSON>()
        for presentation in presentationArray {
            jsonArray.append(presentation.getJSONFromPresentation())
        }
        
        let json = JSON(jsonArray)
        
        return json
    }
    
    func getPresentationsFromFile(fileName: String, fileType: String) -> Array<Presentation> {
        
        var presentationArray: Array<Presentation> = []
        
        if let json = readFile(fileName, fileType: fileType) {
            if let presentationList = json.array {
                for presentationJSON in presentationList {
                    
                    if let presentationFromJSON = Presentation().getPresentationFromJSON(presentationJSON) {
                        presentationArray.append(presentationFromJSON)
                    }
                }
            }
        }
        
        return presentationArray
    }
    
    private func getDocumentsPath() -> String? {
        let documentsPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true).first
        
        return documentsPath
    }
    
    private func getBundlePathForResource(fileName: String, fileType: String) -> String? {
        
        if let bundlePath = NSBundle.mainBundle().pathForResource(fileName, ofType: fileType) {
                    
            return bundlePath
            
        }
        
        return nil
    }
    
    private func getFilePath(fileName: String, fileType: String) -> String? {
        
        if let documentsPath = getDocumentsPath() {
        
            let filePath = documentsPath + "/\(fileName).\(fileType)"
            
            return filePath
        }
        
        return nil
    }
}