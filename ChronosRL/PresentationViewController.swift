//
//  PresentationViewController.swift
//  ChronosRL
//
//  Created by Eduardo Brandalise on 15/04/16.
//  Copyright © 2016 Felipe Lukavei Ferreira. All rights reserved.
//

import UIKit
import WatchConnectivity

class PresentationViewController: UIViewController, WCSessionDelegate {

    //--------------------------------
    // VARIABLES
    
    var timer:Timer?
    var playing = false
    let feedback = FeedbackManager.sharedInstance
    var presentation: Presentation?
    var presentationTime:Int?
    var actualBlock: Block?
    var timeOfBlocks = 0
    var blocksGone = 0
    let device = UIDevice()
    let notificationManager = NotificationsManager()
    
    var session : WCSession?{
        didSet{
            if let session = session{
                session.delegate = self
                session.activateSession()
            }
        }
    }

    
    //--------------------------------
    // OUTLETS
    
    @IBOutlet weak var presentationNameLabel: UILabel!
    @IBOutlet weak var bullet: UIImageView!
    @IBOutlet weak var timerDisplayLabel: UILabel!
    @IBOutlet weak var pauseButton: UIButton!
    @IBOutlet weak var blockNameLabel: UILabel!
    
    //--------------------------------
    // DEFAULT METHODS
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeVariables()
        timer?.startTimer()
        session = WCSession.defaultSession()
        
   
        device.proximityMonitoringEnabled = true
        UIApplication.sharedApplication().idleTimerDisabled = true

        callNotifications()
    
        
        presentation?.getJSONFromPresentation()
        
    
        // Do any additional setup after loading the view.
    }
    
    func callNotifications()
    {
 
        
        notificationManager.getNotificationsFromBlocks(presentation!.getBlocksArray())
        notificationManager.activateNotifications()
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //--------------------------------
    // CUSTOM METHODS

    @IBAction func pauseButtonAction(sender: AnyObject) {
        pauseAndPlay(playing, fromWatch: false)
    
    }
    
    func pauseAndPlay (isPaused: Bool, fromWatch: Bool)
    {
        
        if isPaused{
            timer?.pauseTimer()
            UIApplication.sharedApplication().idleTimerDisabled = false
            playing = false
            notificationManager.cancelNotifications()
            
        } else{
            timer?.startTimer()
            UIApplication.sharedApplication().idleTimerDisabled = true
            playing = true
            
            var notificatonsArray = presentation!.getBlocksArray()
            
            notificatonsArray.removeRange(0..<blocksGone)
            notificationManager.getNotificationsFromBlocks(notificatonsArray)
            notificationManager.activateNotifications()
        }
        if !fromWatch{
            session?.sendMessage(["paused" : isPaused], replyHandler: nil, errorHandler: nil)}
        
    }
    func initializeVariables()
    {
        playing = true
        timer = Timer(time: presentation!.getDuration(), functionInUpdate: self.update)
        
        presentationNameLabel.text = presentation?.getName()
        presentationTime = presentation?.getDuration()
        actualBlock = presentation?.getBlocksArray().first
        blockNameLabel.text = actualBlock!.getLabel()
    }
    
    func update()
    {
        if timer?.getTime() == actualBlock!.getDurationInSeconds() + timeOfBlocks && blocksGone < presentation!.getBlocksArray().count
        {
            
            if actualBlock!.getVibrationStatus(){
                
                feedback.vibrateDevice()
                print("device vibrroooooou")
            }
            if actualBlock!.getSoundStatus() {
                print("device tocoooou")
                feedback.playSound("sound", type: "mp3")
            }
            timeOfBlocks += actualBlock!.getDurationInSeconds()
            blocksGone+=1
            if blocksGone < presentation!.getBlocksArray().count{
            actualBlock = presentation!.getBlocksArray()[blocksGone]
                blockNameLabel.text = actualBlock!.getLabel()
            }
            
        }
        
        timerDisplayLabel.text = getStringHourFormat(timer!.getSecondsString(), minutes: timer!.getMinutesString(), hours: timer!.getHoursString())
    }
    

    
    func getStringHourFormat(seconds:String, minutes:String, hours:String)->String{
        let string = hours + ":" + minutes + ":" + seconds
        return string
    }
    func session(session: WCSession, didReceiveMessage message: [String : AnyObject]) {
        if let _ = message["paused"] as? Bool
        {
            pauseAndPlay(playing, fromWatch: true)
        }
    }
}
