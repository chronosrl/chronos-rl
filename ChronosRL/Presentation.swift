//
//  Presentation.swift
//  Timeer2
//
//  Created by Felipe Ramon de Lara on 4/5/16.
//  Copyright © 2016 Felipe de Lara. All rights reserved.
//

import UIKit
import SwiftyJSON

class Presentation {
    
    private var name: String
    private var blocks: [Block]
    private var totalDurationInSeconds: Int
    private var notifyWatch: Bool
    
    init(){
        self.name = ""
        self.blocks = [Block]()
        self.totalDurationInSeconds = 0
        self.notifyWatch = false
    }
    
    func getName()->String {
        return name
    }
    
    func setName(blockName:String){
        name = blockName
    }
    
    func getBlocksArray()->[Block] {
        
        return blocks
    }
    func setBlock(block:Block){
        blocks.append(block)
    }
    
    func getDuration()->Int {
        return totalDurationInSeconds
    }
    
    func setDuration(durationInSeconds:Int){
        totalDurationInSeconds = durationInSeconds
    }
    
    func getWatchStatus()->Bool {
        return notifyWatch
    }
    
    func setWatchStatus(watchStatus:Bool){
        notifyWatch = watchStatus
    }
    
    
    func getJSONFromPresentation() -> JSON {
        
        var presentationDictionary = Dictionary<String, AnyObject>()
        
        presentationDictionary["name"] = name
        presentationDictionary["totalDurationInSeconds"] = totalDurationInSeconds
        presentationDictionary["notifyWatch"] = notifyWatch
        presentationDictionary["blocks"] = getConvertedBlockArrayForSaving()
        
        let presentationJSON = JSON(presentationDictionary)
        
        return presentationJSON
    }
    
    func getPresentationFromJSON(json: JSON) -> Presentation? {
        name = json["name"].stringValue
        
        if let blockList = json["blocks"].array{
            //blocks.removeAll()
            for block in blockList{
                if let newBlock = Block().getBlockFromJSON(block){
                    self.blocks.append(newBlock)
                }
            }
        }
        
        totalDurationInSeconds = json["totalDurationInSeconds"].intValue
        
        return self
    }
    
    private func getConvertedBlockArrayForSaving() -> Array<Dictionary<String, AnyObject>> {
        
        var blockArray = Array<Dictionary<String, AnyObject>>()
        
        for block in blocks {
            let blockDictionary = block.getDictionaryFromBlock()
            blockArray.append(blockDictionary)
        }
        
        return blockArray
    }
    
    func populateFromFile(){
        let path = NSBundle.mainBundle().pathForResource("Presentations", ofType: "json")
        
        let data: NSData?
        
        do{
            data = try NSData(contentsOfFile: path!, options: .DataReadingMappedIfSafe)
            
            
            let json = JSON(data: data!)
            print("Data:  \(json.arrayObject)")
            
            
            //let presentation = Presentation().getPresentationFromJSON(json)
            print("oi")
        }catch let error as NSError {
            data = nil
            print("Could not open file")
            print("Error: \(error.description)")
        }
    }
}

class Block{
    private var label: String
    private var durationInSeconds: Int
    private var timeIntervalBeginning: Int
    private var soundAtBeginning : Bool
    private var vibrateAtBeginning : Bool
    private var color: Color
  
    
    init(){
        self.label = ""
        self.durationInSeconds = 0
        self.timeIntervalBeginning = 0
        self.soundAtBeginning = false
        self.vibrateAtBeginning = false
        self.color = Color.Blue
    }
    
    func getJSONFromBlock() -> JSON {
        
        var blockDictionary = Dictionary<String, AnyObject>()
        
        blockDictionary["label"] = label
        blockDictionary["durationInSeconds"] = durationInSeconds
        blockDictionary["timeIntervalBegining"] = timeIntervalBeginning
        blockDictionary["vibrateAtBeginning"] = vibrateAtBeginning
        blockDictionary["soundAtBeginning"] = soundAtBeginning
        blockDictionary["color"] = color.getStringValue()
        
        let blockJSON = JSON(blockDictionary)
        
        return blockJSON
    }
    
    func getDictionaryFromBlock() -> Dictionary<String, AnyObject> {
        
        var blockDictionary = Dictionary<String, AnyObject>()
        
        blockDictionary["label"] = label
        blockDictionary["durationInSeconds"] = durationInSeconds
        blockDictionary["timeIntervalBegining"] = timeIntervalBeginning
        blockDictionary["vibrateAtBeginning"] = vibrateAtBeginning
        blockDictionary["soundAtBeginning"] = soundAtBeginning
        blockDictionary["color"] = color.getStringValue()
        
        return blockDictionary
    }
    
    func getBlockFromJSON(json: JSON) -> Block?{
        label = json["label"].stringValue
        durationInSeconds = json["durationInSeconds"].intValue
        timeIntervalBeginning = json["timeIntervalBeginning"].intValue
        soundAtBeginning = json["soundAtBeginning"].boolValue
        vibrateAtBeginning = json["vibrateAtBeginning"].boolValue
        color = Color(stringValue: json["color"].stringValue)
        return self
    }
    
    func getLabel()->String {
        return label
    }
    
    func setLabel(labelText:String){
        label = labelText
    }
    
    func getDurationInSeconds()->Int{
        return durationInSeconds
    }
    
    func setDurationInSeconds(durationInSeconds:Int) {
        
        self.durationInSeconds = durationInSeconds
    }

    func getTimeIntervalBeginning()->Int{
        return timeIntervalBeginning
    }
    
    func setTimeIntervalBeginning(timeBeginning:Int) {
        
        timeIntervalBeginning = timeBeginning
    }
    
    func getVibrationStatus()->Bool {
        return vibrateAtBeginning
    }
    
    func setVibration(vibration:Bool){
        vibrateAtBeginning = vibration
    }
    
    func getSoundStatus()->Bool {
        return soundAtBeginning
    }
    
    func setSoundStatus(soundPlay: Bool) {
        soundAtBeginning = soundPlay
    }
    
    func getBlockColor()->Color{
        
        return color
        
    }
    
    func setBlockColor(blockColor:Color){
        color = blockColor
    }
}

enum Color {
    case Red, Orange, Yellow, Green, Blue, Purple, Black
    
    init(stringValue:String){
        
        switch stringValue {
        case "Red":
            self = .Red
        case "Orange":
            self = .Orange
        case "Yellow":
            self = .Yellow
        case "Green":
            self = .Green
        case "Blue":
            self = .Blue
        case "Purple":
            self = .Purple
        default:
            self = .Black
        }
    }
    func getStringValue()->String{
        switch self {
        case .Red:
            return "Red"
            
        case .Orange:
            return "Orange"
            
        case .Yellow:
            return "Yellow"
            
        case .Green:
            return "Green"
            
        case .Blue:
            return "Blue"
            
        case .Purple:
            return "Purple"
        default:
            return "Black"
            
        }

        
    }
    
    func toUIColor() -> UIColor {
        switch self {
        case .Red:
            return UIColor(red: 179, green: 47, blue: 60, alpha: 1)
            
        case .Orange:
            return UIColor(red: 248, green: 148, blue: 29, alpha: 1)
            
        case .Yellow:
            return UIColor(red: 231, green: 217, blue: 54, alpha: 1)
            
        case .Green:
            return UIColor(red: 57, green: 181, blue: 74, alpha: 1)
            
        case .Blue:
            return UIColor(red: 0, green: 114, blue: 188, alpha: 1)
            
        case .Purple:
            return UIColor(red: 146, green: 39, blue: 143, alpha: 1)
        default:
            return UIColor(red: 0, green: 0, blue: 0, alpha: 0)
            
        }
        
    }
}