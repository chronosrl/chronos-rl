//
//  FeedbackManager.swift
//  ChronosRL
//
//  Created by Leonardo Piovezan on 4/15/16.
//  Copyright © 2016 Felipe Lukavei Ferreira. All rights reserved.
//

import UIKit
import AVFoundation




class FeedbackManager:AnyObject {
    
    
    static let sharedInstance = FeedbackManager()
    
    init()
    {
        
    }

    
    func vibrateDevice()
    {
        AudioServicesPlayAlertSoundWithCompletion(kSystemSoundID_Vibrate) { 
            print("vibrou aeaaea")
        }
        
    }
    
    func playSound(name:String,type:String)
    {
        let soundPath = NSBundle.mainBundle().pathForResource(name, ofType: type)
        
        var audioPlayer = AVAudioPlayer()
        if let _ = soundPath
        {
            let soundURL = NSURL(fileURLWithPath: soundPath!)
            
            do {
                try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
                try AVAudioSession.sharedInstance().setActive(true)
                audioPlayer = try AVAudioPlayer(contentsOfURL: soundURL)
                audioPlayer.prepareToPlay()
                audioPlayer.play()
            } catch let error as NSError{
                print(error.description)
            }
            
            
        }
   
    }
    

    
    
}
