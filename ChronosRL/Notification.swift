//
//  Notification.swift
//  ChronosRL
//
//  Created by Leonardo Piovezan on 4/21/16.
//  Copyright © 2016 Felipe Lukavei Ferreira. All rights reserved.
//

import UIKit

class Notification{
    
    private var title:String?
    private var startDate: NSDate?
    private var deadline: NSDate?
    private var userID:String?
    private var localNotification: UILocalNotification?
    private var timeOfNotification:NSTimeInterval?
    
    init(startDate:NSDate, timeOfNotification: NSTimeInterval,title:String, userid: String) {
        self.startDate = startDate
        self.title = title
        self.userID = userid
        self.timeOfNotification = timeOfNotification
    }
    
    private func createNotificaton()
    {
        calculateTimeOfNotification()
        let notification = UILocalNotification()
        
        notification.alertBody = title
        notification.fireDate = NSDate(timeInterval: timeOfNotification!, sinceDate: startDate!)
        notification.category = "TODO_CATEGORY"
        notification.soundName = UILocalNotificationDefaultSoundName
        localNotification = notification
    }
    
    private func calculateTimeOfNotification()
    {
        deadline = NSDate(timeInterval: timeOfNotification!, sinceDate: startDate!)
        
    }
    
    func cancelNotification()
    {
        
        let notificationsArray = UIApplication.sharedApplication().scheduledLocalNotifications
        if let _ = notificationsArray{
        for notification in notificationsArray!{
            
            if  notification == localNotification!
            {
                
                UIApplication.sharedApplication().cancelLocalNotification(localNotification!)
                return
            }
        }
    }
    }
    
    func startNotification(startDate:NSDate)
    {
        self.startDate = startDate
        
        createNotificaton()
        
        UIApplication.sharedApplication().scheduleLocalNotification(localNotification!)
    }
    
    
    

}
