//
//  NotificationsManager.swift
//  ChronosRL
//
//  Created by Leonardo Piovezan on 4/21/16.
//  Copyright © 2016 Felipe Lukavei Ferreira. All rights reserved.
//

import UIKit

class NotificationsManager {
    
    static let sharedInstance = NotificationsManager()
    
    private var notificationsArray = Array<Notification>()
    
    private func addNotification(notification:Notification)
    {
        notificationsArray.append(notification)
    }
    
    func cancelNotifications()
    {
        for notification in notificationsArray
        {
            notification.cancelNotification()
        }
        notificationsArray.removeAll()
    }
    
    func activateNotifications()
    {
        let date = NSDate()
        for notification in notificationsArray
        {
            notification.startNotification(date)
        }
    }
    
    func getNotificationsFromBlocks(arrayBlocks:Array<Block>)
    {
         var timeAcumulated = 0
        
        for block in arrayBlocks
        {
            timeAcumulated += block.getDurationInSeconds()
            
            let notification = Notification(startDate: NSDate(), timeOfNotification: NSTimeInterval(timeAcumulated), title: block.getLabel(), userid: "1")
            addNotification(notification)
            

        }
    }

}
