//
//  TableViewController.swift
//  ChronosRL
//
//  Created by Eduardo Brandalise on 15/04/16.
//  Copyright © 2016 Felipe Lukavei Ferreira. All rights reserved.
//

import UIKit
import WatchConnectivity
import SwiftyJSON

class TableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating, WCSessionDelegate {

    //--------------------------------
    // VARIABLES
    
    var presentationList = Array<String>()
    var presentations = Array<Presentation>()
    let searchController =  UISearchController(searchResultsController: nil)
    var filteredArrayOfPresentations:[String] = []
    var presentationTableView = UITableView()
    let fileManager = FileManager.sharedInstance
    
    
    //--------------------------------
    // MARK: DEFAULT METHODS
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadTableViewData()
        
        let block = Block()
        block.setLabel("Retortaaaaaaa!!!!!")
        block.getJSONFromBlock()
        
        let tableRectangle = CGRect(x: 0, y: 40, width: self.view.bounds.width, height: self.view.bounds.height)
        presentationTableView = UITableView(frame: tableRectangle)
        
        presentationTableView.delegate = self
        presentationTableView.dataSource = self
        presentationTableView.backgroundColor = UIColor.darkGrayColor()
        
        self.view.addSubview(presentationTableView)
        
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        presentationTableView.tableHeaderView = searchController.searchBar
        sendPresentationsToWatch()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        
        loadTableViewData()
    
        self.presentationTableView.reloadData()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //--------------------------------
    // MARK: CUSTOM METHODS
    
    func loadTableViewData() {
        getPresentations()
        getPresentationNames()
        sendPresentationsToWatch()
    
    }
    
    func sendPresentationsToWatch()
    {
        session = WCSession.defaultSession()
        
        var arrayPresentations = Array<Dictionary<String,AnyObject>>()
        
        for presentation in presentations {
            
            let marksArray = getHapticMarks(presentation)
            let blocksNames = getBlocksNames(presentation)
            let presentationDuration = presentation.getDuration()
            let presentationName = presentation.getName()
            let message:Dictionary<String,AnyObject> = ["presentationTotalTime" : presentationDuration,"notifications":marksArray, "blockNames": blocksNames, "name": presentationName]
            arrayPresentations.append(message)
            
        }
        
        
        do {
            
//            try session?.updateApplicationContext(["presentations":arrayPresentations])
            print("teoricamente conseguiu mandar")
            try session?.sendMessage(["presentations":arrayPresentations], replyHandler: nil, errorHandler: nil)
           // session?.transferUserInfo(["presentations":])
            
        } catch let error as NSError {
            print(error.description)
        }
    }
    
    
    func getPresentations() -> Array<Presentation>{
        presentations = fileManager.getPresentationsFromFile("Presentations", fileType: "json")
        
        return presentations
    }
    
    func getPresentationNames() -> Array<String> {
        
        presentationList = []
        presentations = []
        presentations = getPresentations()
        
        for presentation in presentations {
            
            presentationList.append(presentation.getName())
        }
        print("Estas são as Presentations: \(presentationList)")
        return presentationList
    }
    
    //--------------------------------
    // MARK: TABLE VIEW METHODS
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.active && searchController.searchBar.text != "" {
            return filteredArrayOfPresentations.count
        }
        
        
        
        return presentations.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let identifier = "reusedCell"
        var cell = tableView.dequeueReusableCellWithIdentifier(identifier)
        
        if cell == nil
        {
            cell = UITableViewCell(style: .Subtitle, reuseIdentifier: identifier)
        }
        
        let rowNumber = indexPath.row
        
        let cellText:String
        
        if searchController.active && searchController.searchBar.text != ""
        {
            cellText = filteredArrayOfPresentations[rowNumber]
        } else{
            cellText = presentationList[rowNumber]
        }
        
        cell?.backgroundColor = UIColor.darkGrayColor()
        cell?.textLabel?.text = cellText
        cell?.textLabel?.textColor = UIColor.whiteColor()
        
        return cell!
    }

    
    // FUNÇÃO DO DELEGATE PRA QUANDO A CÉLULA É SELECIONADA.

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        _ = tableView.cellForRowAtIndexPath(indexPath)
        
        self.performSegueWithIdentifier("playPresentation", sender: indexPath)
        
    }
    
    func tableView(tableView: UITableView, editingStyleForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCellEditingStyle {
        return UITableViewCellEditingStyle.Delete
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
        if (editingStyle == UITableViewCellEditingStyle.Delete) {
            let rowNumber = indexPath.row
            
            presentationList.removeAtIndex(rowNumber)
            fileManager.deletePresentation(rowNumber)
            
            print(self.getPresentationNames())
            
            let indexPaths = [indexPath]
            tableView.deleteRowsAtIndexPaths(indexPaths, withRowAnimation: .Automatic)
        }
        
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
     
        return 60
    }
    
    

    func updateSearchResultsForSearchController(searchController: UISearchController) {
        
        filteredArrayOfPresentations = presentationList.filter{ presentation in
            return presentation.lowercaseString.containsString(searchController.searchBar.text!.lowercaseString)
        }
        presentationTableView.reloadData()
    }
    //--------------------------------
    // CUSTOM METHODS
    
    func session(session: WCSession, didFinishUserInfoTransfer userInfoTransfer: WCSessionUserInfoTransfer, error: NSError?) {
        print(error?.description)
        print("Naruto deixou chakra aqui!")
    }

    var session : WCSession?{
        didSet{
            if let session = session{
                session.delegate = self
                session.activateSession()
            }
        }
    }
    
    
    func sendMessage(presentation: Presentation) {
        session = WCSession.defaultSession()
        
        let marksArray = getHapticMarks(presentation)
        let blocksNames = getBlocksNames(presentation)
        let presentationDuration = presentation.getDuration()
        let message = ["presentationTotalTime" : presentationDuration,"notifications":marksArray, "blockNames": blocksNames]
        
        
        transferInfo(message as! Dictionary<String, AnyObject>)
        
    }
    
    func transferInfo (dataDictionary: Dictionary<String, AnyObject>)
    {
        session?.transferUserInfo(dataDictionary)
      
        // session?.transferCurrentComplicationUserInfo(dataDictionary)
        
    }
    
    func getHapticMarks(presentation:Presentation)->Array<Int>
    {
        
        var marksArray = Array<Int>()
        
        var relativeDuration = 0
        
        for block in presentation.getBlocksArray(){
            marksArray.append(block.getDurationInSeconds()+relativeDuration)
            relativeDuration += block.getDurationInSeconds()
        }
        
        return marksArray
    }
    
    func getBlocksNames(presentation:Presentation)->Array<String>{
        
        var blocksNames = Array<String>()
        
        for block in presentation.getBlocksArray(){
            blocksNames.append(block.getLabel())
        }
        return blocksNames
        
    }


    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        
            if segue.identifier == "playPresentation"{
                let nextController = segue.destinationViewController as! PresentationViewController
                var presentation = Presentation()
                
                presentation = presentations[(sender as! NSIndexPath).row]
                nextController.presentation = presentation
                sendMessage(presentation)
            
        }
    }

}
