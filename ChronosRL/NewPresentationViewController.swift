//
//  ViewController.swift
//  TestesMainChallenge
//
//  Created by Leonardo Piovezan on 4/5/16.
//  Copyright © 2016 Leonardo Piovezan. All rights reserved.
//

import UIKit
import WatchConnectivity

class NewPresentationViewController: UIViewController, UIScrollViewDelegate, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, WCSessionDelegate {
    
    // MARK: VARIABLES
//    var myView = UIView()
    
    var touchedBlock: Bool?
    var arrayViews = Array<UIView>()
    var selectedBlock = UIView()
    var scrollView: TimelineScrollView!
    var intersection:CGRect?
    var tableView = UITableView()
    var presentationName = "Presentation Title"
    var sessionName: String?
    var presentationTotalTime: Float = 600.0
    var temporaryPresentationTotalTime:Int = 0
    var temporaryTimeArray: Array<Int> = [0,0,0]
    
    @IBOutlet weak var presentationNameButton: UIButton!
    @IBOutlet weak var presentationTimeButton: UIButton!
    @IBOutlet weak var blockEditionView: UIView!
    @IBOutlet weak var presentationNameEditingView: UIView!
    @IBOutlet weak var presentationTimeEditionView: UIView!
    @IBOutlet weak var playPresentationButton: UIButton!
    
    // OUTLETS BLOCK EDITION
    @IBOutlet weak var blockNameTextField: UITextField!
    @IBOutlet weak var confirmBlockEditionButton: UIButton!
    @IBOutlet weak var blockTimeSlider: UISlider!
    @IBOutlet weak var sessionTimeLabel: UILabel!
    @IBOutlet weak var vibrateSwitch: UISwitch!
    
    // OUTLETS PRESENTATION NAME EDITION
    @IBOutlet weak var presentationNameTextField: UITextField!
    @IBOutlet weak var soundSwitch: UISwitch!
    
    
    // OUTLETS PRESENTATION TIME EDITION
    @IBOutlet weak var presentationTimeLabel: UILabel!
    @IBOutlet weak var presentationTimeSelectionPickerView: UIPickerView!
    
    
    //MARK: CONNECTIVITY
    
    var session : WCSession?{
        didSet{
            if let session = session{
                session.delegate = self
                session.activateSession()
            }
        }
    }
    
    
    func sendMessage(presentation: Presentation) {
        session = WCSession.defaultSession()
    
        let marksArray = getHapticMarks(presentation)
        let blocksNames = getBlocksNames(presentation)
        let presentationDuration = presentation.getDuration()
        let message = ["presentationTotalTime" : presentationDuration,"notifications":marksArray, "blockNames": blocksNames]
        
            
        transferInfo(message as! Dictionary<String, AnyObject>)
        
    }
    
    func transferInfo (dataDictionary: Dictionary<String, AnyObject>)
    {
        session?.transferUserInfo(dataDictionary)
       // session?.transferCurrentComplicationUserInfo(dataDictionary)
      
    }

    func getHapticMarks(presentation:Presentation)->Array<Int>
    {
        
        var marksArray = Array<Int>()
        
        var relativeDuration = 0
        
        for block in presentation.getBlocksArray(){
            marksArray.append(block.getDurationInSeconds()+relativeDuration)
            relativeDuration += block.getDurationInSeconds()
        }
        
        return marksArray
    }
    
    func getBlocksNames(presentation:Presentation)->Array<String>{
        
        var blocksNames = Array<String>()
        
        for block in presentation.getBlocksArray(){
            blocksNames.append(block.getLabel())
        }
        return blocksNames
        
    }
    
    // MARK: DEFAULT METHODS
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
      
        temporaryPresentationTotalTime = Int(presentationTotalTime)
        presentationTimeLabel.text = convertSecondsToHourMinuteSecondFormat(temporaryPresentationTotalTime)
        
        //------------ HIDE POPUPS ------------
        presentationTimeEditionView.hidden = true
        presentationNameEditingView.hidden = true
        
        //------------ PRESENTATION NAME AND TIME BUTTONS ------------
        
        presentationNameButton.setTitle(presentationName, forState: .Normal)
        
        let presentationTimeConverted: String = convertSecondsToHourMinuteSecondFormat(Int(presentationTotalTime))
        presentationTimeButton.setTitle(presentationTimeConverted, forState: .Normal)
        
        //------------ PICKERVIEW ------------
        presentationTimeSelectionPickerView.delegate = self
        presentationTimeSelectionPickerView.dataSource = self
        
        //------------ SLIDERS ------------
        blockTimeSlider.maximumValue = presentationTotalTime
        blockTimeSlider.minimumValue = 1.0
        blockTimeSlider.continuous = true

        
        //------------ BLOCK NAME TEXTFIELD ------------
        blockNameTextField.borderStyle = UITextBorderStyle.RoundedRect
        blockNameTextField.placeholder = "Session Name Here"
        blockNameTextField.autocorrectionType = UITextAutocorrectionType.No
        blockNameTextField.returnKeyType = UIReturnKeyType.Done
        blockNameTextField.keyboardType = UIKeyboardType.Default
        blockNameTextField.delegate = self
        
        //------------ PRESENTATION NAME TEXTFIELD ------------
        presentationNameTextField.borderStyle = UITextBorderStyle.RoundedRect
        presentationNameTextField.placeholder = "Presentation Name Here"
        presentationNameTextField.autocorrectionType = UITextAutocorrectionType.Yes
        presentationNameTextField.returnKeyType = UIReturnKeyType.Done
        presentationNameTextField.keyboardType = UIKeyboardType.Default
        presentationNameTextField.delegate = self
        
        //------------ PLAY BUTTON ------------
        playPresentationButton.showsTouchWhenHighlighted = true
        
        
        touchedBlock = false
        //Timeline Settings
        scrollView = TimelineScrollView(frame: CGRect(x:0 , y: CGRectGetHeight(view.bounds) - 250, width: CGRectGetWidth(view.bounds), height: 150), function: showBlockEditor)
        
        scrollView.backgroundColor = UIColor.grayColor()
      
        scrollView.contentSize = CGSize(width: 1000, height: 50)
        scrollView.clipsToBounds = true
        
        scrollView.delegate = self
        scrollView.getFuncFromSuperview(showBlockEditor)
        blockEditionView.hidden = true
        view.addSubview(scrollView)
        view.sendSubviewToBack(scrollView)
        

        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
     
        
    }

    
    // MARK: CUSTOM METHODS
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        for touch in touches {
            let position = touch.locationInView(self.view)
           
            for block in arrayViews{
                if (CGRectContainsPoint(block.frame, position) && (block.superview?.isMemberOfClass(UIView))!){
                    touchedBlock = true
                    selectedBlock = block
                    
                    UIView.animateWithDuration(0.2, delay: 0, options: .TransitionCurlUp, animations: {
                        block.transform = CGAffineTransformMakeScale(1.3, 1.3)
                        }, completion: { finished in
                    })
                    self.scrollView.darken()
                    break
                }else{
                    touchedBlock = false
                }
            }
        }
    }
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        for touch in touches {
            
            if(touchedBlock!){
                selectedBlock.center = CGPointMake(touch.locationInView(self.view).x, touch.locationInView(self.view).y);
                
            }
        }
    }
    
    //?
    func getAdjustedBlockPositionInScroll(position: CGRect) -> CGRect
    {
        let position = CGRect(x: selectedBlock.frame.origin.x + self.scrollView.contentOffset.x, y: selectedBlock.frame.origin.y + selectedBlock.frame.size.height - position.size.height, width: selectedBlock.frame.size.width, height: selectedBlock.frame.height)

        return position
        
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        UIView.animateWithDuration(0.3, delay: 0, options: .TransitionCurlUp, animations: {
            self.selectedBlock.transform = CGAffineTransformMakeScale(1, 1)

            if !(CGRectIntersection(self.scrollView.frame, self.selectedBlock.frame).isEmpty)
            {
                self.intersection = CGRectIntersection(self.scrollView.frame, self.selectedBlock.frame)
                
                print(self.intersection)
                
              //  self.selectedBlock.frame = self.getAdjustedBlockPositionInScroll(self.intersection!)
            }
            }, completion: { finished in
                
                if let _ = self.intersection{
                    
                    self.selectedBlock.removeFromSuperview()
                    self.selectedBlock.frame = self.convertScreenCoordinateToScrollView(self.intersection!)
                    self.scrollView.addSubview(self.selectedBlock)
                    self.intersection = nil
                    self.scrollView.blockViewArray.append(self.selectedBlock)
                    
                    self.scrollView.moveBlocksByItsRight(self.selectedBlock,convertedRect: self.selectedBlock.frame)
                    self.scrollView.sortBlockArray()
                    self.scrollView.adjustBlockGaps()

                    
                    
                }
        })
        self.scrollView.lighten()
        touchedBlock = false
    }
    
    func convertScreenCoordinateToScrollView(coordinate: CGRect) -> CGRect
    {
        let newRect = CGRect(x: coordinate.origin.x + scrollView.contentOffset.x, y: 0, width: selectedBlock.frame.size.width, height: selectedBlock.frame.size.height)
        
        return newRect
    }
    
    
    
    @IBAction func createNewView(sender: AnyObject) {
        
        let newView = UIView(frame: CGRect(x: self.view.center.x , y: self.view.center.y, width: 100, height: 100))
        
        newView.backgroundColor = getRandomColor() //UIColor.darkGrayColor()
        
        let block = Block()
        block.setLabel("new title")
        
        newView.block = block
        arrayViews.append(newView)
        
        self.view.addSubview(newView)
    }
    

    func getRandomColor() -> UIColor{
        
        let randomRed:CGFloat = CGFloat(drand48())
        
        let randomGreen:CGFloat = CGFloat(drand48())
        
        let randomBlue:CGFloat = CGFloat(drand48())
        
        return UIColor(red: randomRed, green: randomGreen, blue: randomBlue, alpha: 1.0)
        
    }
    
    
    
    @IBAction func setPresentationNameAction(sender: AnyObject) {
        
        presentationNameEditingView.hidden = false
    }
    
    
    @IBAction func setPresentationTime(sender: AnyObject) {
        presentationTimeEditionView.hidden = false
        print("Entrou mas não mostrou")

    }
    
    @IBAction func confirmPresentationNameEditing(sender: AnyObject) {
        
        presentationNameButton.setTitle(presentationName, forState: .Normal)
        
        if presentationNameTextField.text == "" {
            let alertController = UIAlertController(title: "Empty Name", message: "Please, set a name for the presentation.", preferredStyle: .Alert)
            
            alertController.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
            self.presentViewController(alertController, animated: true, completion: nil)
            
        } else {
            presentationName = presentationNameTextField.text!
            presentationNameButton.setTitle(presentationName, forState: .Normal)
            presentationNameEditingView.hidden = true
        }
    }
    
    @IBAction func confirmEditPresentationTime(sender: AnyObject) {
        
        let hourInSeconds = temporaryTimeArray[0]*3600
        let minutesInSeconds = temporaryTimeArray[1]*60
        let seconds = temporaryTimeArray[2]
        
        presentationTotalTime = Float(seconds+minutesInSeconds+hourInSeconds)
        presentationTimeEditionView.hidden = true
        
        presentationTimeButton.setTitle(convertSecondsToHourMinuteSecondFormat(Int(presentationTotalTime)), forState: .Normal)
        blockTimeSlider.maximumValue = presentationTotalTime
        
    }
    
    
//    func getRelativeBlockWidh(selectedBlock: UIView) -> CGFloat{
//        
//    }
    
    
    @IBAction func confirmBlockEditionAction(sender: AnyObject) {
        
        
        let text = blockNameTextField.text
        if let _ = text{
            
            selectedBlock.block?.setLabel(text!)
        }
        
        selectedBlock.frame = CGRectMake(selectedBlock.frame.origin.x, selectedBlock.frame.origin.y, CGFloat( blockTimeSlider.value * 3), 100) //CGRectMake(23, 50, myview.frame.size.width, myview.frame.size.height);

//        selectedBlock.frame.width = (CGFloat); blockTimeSlider.value * 3
        
        selectedBlock.block?.setVibration(vibrateSwitch.on)
        selectedBlock.block?.setSoundStatus(soundSwitch.on)
        selectedBlock.block?.setDurationInSeconds(Int(blockTimeSlider.value))
        
        self.scrollView.sortBlockArray()
        self.scrollView.adjustBlockGaps()
        
        blockEditionView.hidden = true
    }
    
    // MARK: PICKERVIEW METHODS
    
    let pickerData:[[String]] = [["0","1", "2", "3", "4", "5", "6","7","8","9"],
                                 ["0","1", "2", "3", "4", "5", "6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","52","53","54","55","56","57","58","59"],
                                 ["0","1", "2", "3", "4", "5", "6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","52","53","54","55","56","57","58","59"]]
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 3
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {  
        
        return pickerData[component].count
        
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[component][row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        
        
        if component == 0 {
            
            let value = pickerData[component][row]
            temporaryTimeArray[0] = Int(value)!
            
            let hours = String(format: "%02d", temporaryTimeArray[0])
            let minutes = String(format: "%02d", temporaryTimeArray[1])
            let seconds = String(format: "%02d", temporaryTimeArray[2])
            
            presentationTimeLabel.text = "\(hours):\(minutes):\(seconds)"
        }
        
        if component == 1 {
            
            let value = pickerData[component][row]
            temporaryTimeArray[1] = Int(value)!
            
            let hours = String(format: "%02d", temporaryTimeArray[0])
            let minutes = String(format: "%02d", temporaryTimeArray[1])
            let seconds = String(format: "%02d", temporaryTimeArray[2])
            
            presentationTimeLabel.text = "\(hours):\(minutes):\(seconds)"
        }
        
        if component == 2 {
            
            let value = pickerData[component][row]
            temporaryTimeArray[2] = Int(value)!
            
            let hours = String(format: "%02d", temporaryTimeArray[0])
            let minutes = String(format: "%02d", temporaryTimeArray[1])
            let seconds = String(format: "%02d", temporaryTimeArray[2])
            
            presentationTimeLabel.text = "\(hours):\(minutes):\(seconds)"
        }
    }
    
    func pickerView(pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        
        let titleData = pickerData[component][row]
        let myTitle = NSAttributedString(string: titleData, attributes: [NSFontAttributeName:UIFont(name: "Georgia", size: 15.0)!,NSForegroundColorAttributeName:UIColor.whiteColor()])
        return myTitle
    }
    
    // MARK: SLIDER METHODS

    @IBAction func blockTimeSliderValueUpdate(sender: AnyObject) {
    
        refreshLabelSlider()
    }
    
    func refreshLabelSlider()
    {
        let currentValue = Int(blockTimeSlider.value)
        let convertedTime = convertSecondsToHourMinuteSecondFormat(currentValue)
        
        sessionTimeLabel.text = "\(convertedTime)"
    }
    
    func convertSecondsToHourMinuteSecondFormat (seconds: Int) -> String {
        
        var secondsTime = 0
        var minutesTime = 0
        var hoursTime = 0
        
        hoursTime = seconds/3600
        minutesTime = (seconds - (hoursTime)*3600)/60
        secondsTime = (seconds - (hoursTime)*3600 - minutesTime*60)
        
        let hours = String(format: "%02d", hoursTime)
        let minutes = String(format: "%02d", minutesTime)
        let seconds = String(format: "%02d", secondsTime)
        
        let convertedTime = "\(hours):\(minutes):\(seconds)"
        
        return convertedTime
    }
    
    // MARK: TEXTFIELD DELEGATE METHODS
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true
    }
    
    
    func showBlockEditor(novoblock: UIView)

    {
        
        if (((self.scrollView.touchedBlock)) != nil) {
           
        
        
        self.blockNameTextField.text = novoblock.block?.getLabel()
        
        if let _ = novoblock.block?.getVibrationStatus()
        {
            vibrateSwitch.setOn((novoblock.block?.getVibrationStatus())!, animated: false)
        } else{
            vibrateSwitch.setOn(true, animated: false)
        }
        
        if let _ = novoblock.block?.getSoundStatus() {
            soundSwitch.setOn((novoblock.block?.getSoundStatus())!, animated: false)
        } else {
            soundSwitch.setOn(true, animated: false)
        }
        
        if let value = novoblock.block?.getDurationInSeconds() {
         blockTimeSlider.setValue(Float(value), animated: false)
        } else{
            blockTimeSlider.setValue(0, animated: false)
        }
        refreshLabelSlider()
        
        self.selectedBlock = novoblock
        blockEditionView.hidden = false
        
        }
    }

    
    @IBAction func vibrateAction(sender: AnyObject) {
        FeedbackManager.sharedInstance.vibrateDevice()
  
    }
    
    @IBAction func soundAction(sender: AnyObject) {
        
        FeedbackManager.sharedInstance.playSound("sound", type: "mp3")
        
    }
    
    func removeBlockFromArray(blockToRemove:UIView)
    {
        for (index,block) in arrayViews.enumerate()
        {
            if block.isEqual(blockToRemove)
            {
                arrayViews.removeAtIndex(index)
                return
            }
            
        }
    }
    
    @IBAction func deleteBlock(sender: AnyObject) {
        scrollView.removeBlockFromArray(selectedBlock)
        self.removeBlockFromArray(selectedBlock)
        selectedBlock.removeFromSuperview()
        blockEditionView.hidden = true

        scrollView.adjustBlockGaps()
        scrollView.sortBlockArray()


    }
    
    
    @IBAction func preparePresentationToPlay(sender: AnyObject) {
     
        
        
    }

    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if scrollView.blockViewArray == [] {
            let alertController = UIAlertController(title: "No Sessions", message: "Please, add at least one session to your presentation.", preferredStyle: .Alert)
            
            alertController.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
            self.presentViewController(alertController, animated: true, completion: nil)
            
        } else {
            let fileManager = FileManager.sharedInstance
            let nextController = segue.destinationViewController as! PresentationViewController
            
            if segue.identifier == "presentationPlay"{
                let presentation = Presentation()
                
                presentation.setDuration(Int(presentationTotalTime))
                
                for block in scrollView.blockViewArray{
                    
                    presentation.setBlock(block.block!)
                }
                    
                presentation.setName(presentationName)
                fileManager.saveNewPresentation(presentation)
                nextController.presentation = presentation
                sendMessage(presentation)
                
            }
        }
    }
}