//
//  PresentationInterfaceController.swift
//  ChronosRL
//
//  Created by Eduardo Brandalise on 07/04/16.
//  Copyright © 2016 Felipe Lukavei Ferreira. All rights reserved.
//

import UIKit
import WatchKit
import Foundation
import WatchConnectivity


class PresentationInterfaceController: WKInterfaceController, WCSessionDelegate {
    
    
    // CLASS VARIABLES
    
    var startTime = NSTimeInterval()
    var elapsedTime = NSTimeInterval()
    var timer = NSTimer()
    var timeAtPause = NSTimeInterval()
    var totalTime = NSTimeInterval()
    var paused = false
    var currentSession:String = "Introduction"
    var blockNames = Array<String>()
    var hapticNotificationsTimes = Array<Int>()
    var blocksGone = 0
    var totalPresentationTime = NSTimeInterval()
    
    var session : WCSession?{
        didSet{
            if let session = session{
                session.delegate = self
                session.activateSession()
            }
        }
    }

    // OUTLETS
    @IBOutlet var sessionLabel: WKInterfaceLabel!
    
    @IBOutlet var timerDisplay: WKInterfaceLabel!
    
    @IBOutlet var pauseButton: WKInterfaceButton!

    // MARK: DEFAULT METHODS
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        session = WCSession.defaultSession()
        print(context)
        if let infoDictionary = context as? [String : AnyObject]
        {
            if let totalTimeFromContext = infoDictionary["presentationTotalTime"] as? NSTimeInterval
            {
                print(totalTimeFromContext)
                totalPresentationTime = totalTimeFromContext
            }
        
        blockNames = infoDictionary["blockNames"] as! Array<String>
        
        hapticNotificationsTimes = infoDictionary["notifications"] as! Array<Int>
        
        print(totalTime)
        print(blockNames)
        print(hapticNotificationsTimes)
        
        
        sessionLabel.setText(blockNames.first)
        
        startTimer()
        }

    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        // Calls the method to start the timer.
        
        
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    // MARK: CUSTOM METHODS

    func startTimer() {
        
        timer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: #selector(PresentationInterfaceController.updateTime), userInfo: nil, repeats: true)
        
        startTime = NSDate.timeIntervalSinceReferenceDate()
        
        if paused == true {
            
            pauseButton.setTitle("PAUSE")
            paused = false
        }
        
        timerDisplay.setTextColor(UIColor(red:0.50, green:0.82, blue:0.74, alpha:1.0))
        
        WKInterfaceDevice.currentDevice().playHaptic(.Start)
    }
    
    @IBAction func pauseTimeButton() {
        pauseAndPlay(false)
    }
    
    func pauseAndPlay (fromPhone: Bool)
    {
        if paused == true {
            startTimer()
            paused = false
            return
        }
            
        else{
            timeAtPause = totalTime
            timer.invalidate()
            pauseButton.setTitle("RESUME")
            paused = true
            
            timerDisplay.setTextColor(UIColor(red:0.73, green:0.29, blue:0.39, alpha:1.0))
        }
        if !fromPhone{
            session?.sendMessage(["paused":paused], replyHandler: nil, errorHandler: nil)}
    }
    
    func stopTimer() {
        
        timerDisplay.setTextColor(UIColor(red:0.73, green:0.29, blue:0.39, alpha:1.0))
        timer.invalidate()
    }

    func updateTime() {
        
        //print("---------------- START ---------------")
        
        let currentTime = NSDate.timeIntervalSinceReferenceDate()
        
        //Find the difference between current time and start time.
        
        elapsedTime = currentTime - startTime + timeAtPause
        
        totalTime = elapsedTime
        
        //calculate the minutes in elapsed time.
    
        let hours = Int(elapsedTime / 3600)
        
        elapsedTime -= (NSTimeInterval(hours * 3600))
        
        //calculate the seconds in elapsed time.
        
        let minutes = Int(elapsedTime / 60)
        
        elapsedTime -= NSTimeInterval(minutes * 60)
        
        //find out the fraction of milliseconds to be displayed.
        
        let seconds = Int(elapsedTime)

        
        //add the leading zero for minutes, seconds and millseconds and store them as string constants
      print(totalTime)
        if blocksGone < hapticNotificationsTimes.count {
            if Int(totalTime) == hapticNotificationsTimes[blocksGone]
            {
                WKInterfaceDevice.currentDevice().playHaptic(.Notification)
                print("teoricamente fez um haptic")
              
                blocksGone += 1
                
                if blocksGone < blockNames.count
                {
                    sessionLabel.setText(blockNames[blocksGone])
                }
            }
        }
        let stringHours = String(format: "%02d", hours)
        let stringMinutes = String(format: "%02d", minutes)
        let stringSeconds = String(format: "%02d", seconds)
        
        //concatenate hours, minutes and seconds as assign it to the UILabel
        
        timerDisplay.setText("\(stringHours):\(stringMinutes):\(stringSeconds)")
        
        if Int(totalTime) >= Int(totalPresentationTime) {
            stopTimer()
        }
    }
    
    func session(session: WCSession, didReceiveMessage message: [String : AnyObject]) {
        if let _ = message["paused"] as? Bool
        {
            pauseAndPlay(true)
        }
    }
}