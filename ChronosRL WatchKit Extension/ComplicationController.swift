//
//  ComplicationController.swift
//  ChronosRL WatchKit Extension
//
//  Created by Felipe Lukavei Ferreira on 4/6/16.
//  Copyright © 2016 Felipe Lukavei Ferreira. All rights reserved.
//
import ClockKit
import WatchConnectivity

struct TimeMark {
    var time: NSDate
    var name: String
}

enum complicationType
{
    case UtilitarianLarge
    case UtilitarianSmall
    case ModularLarge
    
}
var TimeMarks = [
    TimeMark(time: inicio.dateByAddingTimeInterval(0) , name: "Começo"),
    TimeMark(time: inicio.dateByAddingTimeInterval(20) , name: "Meio"),
    TimeMark(time: inicio.dateByAddingTimeInterval(40) , name: "Fim")]
var update = NSDate()
var timers = [TimeMarks[0].time, TimeMarks[1].time, TimeMarks[2].time]
var time = 0
var inicio = NSDate()

class ComplicationController: NSObject, CLKComplicationDataSource, WCSessionDelegate {
    
    // MARK: - Metodo que gera template
    
    func geraTemplate(mark : TimeMark, tipoDaComplication: complicationType) -> CLKComplicationTemplate {
        let template = CLKComplicationTemplate()
        if tipoDaComplication == complicationType.ModularLarge
        {
            let template = CLKComplicationTemplateModularLargeTallBody()
            template.headerTextProvider = CLKSimpleTextProvider(text:mark.name)
            template.bodyTextProvider = CLKRelativeDateTextProvider(date: mark.time, style: .Timer, units: .Second)
            return template
        }
        else if tipoDaComplication == complicationType.UtilitarianLarge
        {
            let template = CLKComplicationTemplateUtilitarianLargeFlat()
            template.textProvider = CLKRelativeDateTextProvider(date: mark.time, style: .Timer, units: .Second)
            return template
        }
        else if tipoDaComplication == complicationType.UtilitarianSmall
        {
            let template = CLKComplicationTemplateUtilitarianSmallFlat()
            template.textProvider = CLKSimpleTextProvider(text: "\(mark.name)")
            return template
            
        }
        return template
    }
    
    
    // MARK: - Timeline Configuration
    
    func getSupportedTimeTravelDirectionsForComplication(complication: CLKComplication, withHandler handler: (CLKComplicationTimeTravelDirections) -> Void) {
        handler([.Forward, .Backward])
    }
    
    func getTimelineStartDateForComplication(complication: CLKComplication, withHandler handler: (NSDate?) -> Void) {
        handler(NSDate(timeIntervalSinceNow: -(60*60)))
    }
    
    func getTimelineEndDateForComplication(complication: CLKComplication, withHandler handler: (NSDate?) -> Void) {
        handler(NSDate(timeIntervalSinceNow: 60*60))
    }
    
    func getPrivacyBehaviorForComplication(complication: CLKComplication, withHandler handler: (CLKComplicationPrivacyBehavior) -> Void) {
        handler(.HideOnLockScreen)
    }
    
    // MARK: - Timeline Population
    
    func getCurrentTimelineEntryForComplication(complication: CLKComplication, withHandler handler: ((CLKComplicationTimelineEntry?) -> Void)) {
        // Call the handler with the current timeline entry
        print("next")
        var marks = TimeMarks
        var markToShow = TimeMarks.first!
        do {
            for mark in marks
            {
                if mark.time.timeIntervalSinceNow > NSDate().timeIntervalSinceNow
                {
                    marks.removeFirst()
                }
                else{
                    markToShow = mark
                }
            }
        }
        
        switch (complication.family) {
        case CLKComplicationFamily.ModularLarge :
            handler(CLKComplicationTimelineEntry(date: markToShow.time, complicationTemplate: geraTemplate(markToShow, tipoDaComplication: complicationType.ModularLarge)))
        case CLKComplicationFamily.UtilitarianLarge :
            handler(CLKComplicationTimelineEntry(date: markToShow.time, complicationTemplate: geraTemplate(markToShow, tipoDaComplication: complicationType.UtilitarianLarge)))
        case CLKComplicationFamily.UtilitarianSmall :
            handler(CLKComplicationTimelineEntry(date: markToShow.time, complicationTemplate: geraTemplate(markToShow, tipoDaComplication: complicationType.UtilitarianSmall)))
            
        default:
            handler(nil)
        }
    }
    
    
    
    
    func getTimelineEntriesForComplication(complication: CLKComplication, beforeDate date: NSDate, limit: Int, withHandler handler: (([CLKComplicationTimelineEntry]?) -> Void)) {
        // Call the handler with the timeline entries prior to the given date
        switch (complication.family) {
        case CLKComplicationFamily.ModularLarge :
            
            var entries : [CLKComplicationTimelineEntry] = []
            for mark in TimeMarks {
                if mark.time.compare(date) == .OrderedAscending && entries.count < limit {
                    entries += [CLKComplicationTimelineEntry(date: mark.time, complicationTemplate: geraTemplate(mark, tipoDaComplication: complicationType.ModularLarge))]
                    print("beforeDate [\(mark.name)]")
                }
            }
            handler(entries)
            
        case CLKComplicationFamily.UtilitarianLarge :
            
            var entries : [CLKComplicationTimelineEntry] = []
            for mark in TimeMarks {
                if mark.time.timeIntervalSinceNow < date.timeIntervalSinceNow && entries.count < limit {
                    entries += [CLKComplicationTimelineEntry(date: mark.time, complicationTemplate: geraTemplate(mark, tipoDaComplication: complicationType.UtilitarianLarge))]
                }
            }
            handler(entries)
            
        case CLKComplicationFamily.UtilitarianSmall :
            
            var entries : [CLKComplicationTimelineEntry] = []
            for mark in TimeMarks {
                if mark.time.timeIntervalSinceNow < date.timeIntervalSinceNow && entries.count < limit {
                    entries += [CLKComplicationTimelineEntry(date: mark.time, complicationTemplate: geraTemplate(mark, tipoDaComplication: complicationType.UtilitarianSmall))]
                }
            }
            handler(entries)
        default:
            handler(nil)
        }
    }
    
    func getTimelineEntriesForComplication(complication: CLKComplication, afterDate date: NSDate, limit: Int, withHandler handler: (([CLKComplicationTimelineEntry]?) -> Void)) {        // Call the handler with the timeline entries after to the given date
        switch (complication.family) {
        case CLKComplicationFamily.ModularLarge :
            
            var entries : [CLKComplicationTimelineEntry] = []
            for mark in TimeMarks {
                if mark.time.compare(date) == .OrderedDescending && entries.count < limit  {
                    entries += [CLKComplicationTimelineEntry(date:mark.time, complicationTemplate: geraTemplate(mark, tipoDaComplication: complicationType.ModularLarge))]
                    print("afterDate [\(mark.name)]")
                }
            }
            handler(entries)
            
        case CLKComplicationFamily.UtilitarianLarge :
            
            var entries : [CLKComplicationTimelineEntry] = []
            for mark in TimeMarks {
                if mark.time.timeIntervalSinceNow > date.timeIntervalSinceNow && entries.count < limit  {
                    entries += [CLKComplicationTimelineEntry(date:mark.time, complicationTemplate: geraTemplate(mark, tipoDaComplication: complicationType.UtilitarianLarge))]
                }
            }
            handler(entries)
            
        case CLKComplicationFamily.UtilitarianSmall :
            
            var entries : [CLKComplicationTimelineEntry] = []
            for mark in TimeMarks {
                if mark.time.timeIntervalSinceNow > date.timeIntervalSinceNow && entries.count < limit  {
                    entries += [CLKComplicationTimelineEntry(date:mark.time, complicationTemplate: geraTemplate(mark, tipoDaComplication: complicationType.UtilitarianSmall))]
                }
            }
            handler(entries)
        default:
            handler(nil)
        }
    }
    
    // MARK: - Update Scheduling
    
    func getNextRequestedUpdateDateWithHandler(handler: (NSDate?) -> Void) {
        // Call the handler with the date when you would next like to be given the opportunity to update your complication content
        print("update \(NSDate())")
        time+=1
        if time > timers.count
        {
            time-=1
            return
        }
        update = NSDate()
        print(update)
        handler(timers[time])
    }
    
    
    
    
    // MARK: - Placeholder Templates
    
    func getPlaceholderTemplateForComplication(complication: CLKComplication, withHandler handler: (CLKComplicationTemplate?) -> Void) {
        // This method will be called once per supported complication, and the results will be cached
        switch (complication.family) {
        case CLKComplicationFamily.ModularLarge :
            let template = CLKComplicationTemplateModularLargeTallBody()
            template.bodyTextProvider = CLKSimpleTextProvider(text: "00:00:00")
            template.headerTextProvider = CLKSimpleTextProvider(text: "defaultName")
            handler(template)
            
        case CLKComplicationFamily.UtilitarianLarge :
            let template = CLKComplicationTemplateUtilitarianLargeFlat()
            template.textProvider = CLKSimpleTextProvider(text: "00:00:00")
            handler(template)
            
        case CLKComplicationFamily.UtilitarianSmall :
            let template = CLKComplicationTemplateUtilitarianSmallFlat()
            template.textProvider = CLKSimpleTextProvider(text: "defaultName")
            handler(template)
        default:
            handler(nil)
        }
        
    }
    
    
    
    //MARK: SessionData
    
    var session : WCSession?{
        didSet{
            if let session = session{
                session.delegate = self
                session.activateSession()
            }
        }
    }
    
    func session(session: WCSession, didReceiveApplicationContext applicationContext: [String : AnyObject]) {
        TimeMarks = applicationContext["TimeMarks"] as! [TimeMark]
        inicio = applicationContext["inicio"] as! NSDate
        print("didReceiveApplicationContext \(applicationContext)")
        var inOrderTimes = [NSDate().dateByAddingTimeInterval(9999999)]
        for i in 0 ... TimeMarks.count
        {
            for time in TimeMarks
            {
                if time.time.compare(inOrderTimes[i]) == .OrderedAscending
                {
                    inOrderTimes[i] = time.time
                }
            }
        }
        timers = inOrderTimes

    }
    
}
