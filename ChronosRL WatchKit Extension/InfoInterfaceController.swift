//
//  InfoInterfaceController.swift
//  ChronosRL
//
//  Created by Eduardo Brandalise on 14/04/16.
//  Copyright © 2016 Felipe Lukavei Ferreira. All rights reserved.
//

import WatchKit
import WatchConnectivity


class InfoInterfaceController: WKInterfaceController, WCSessionDelegate {
    
    var session : WCSession?{
        didSet{
            if let session = session{
                session.delegate = self
                session.activateSession()
            }
        }
    }
    
    func session(session: WCSession, didReceiveUserInfo userInfo: [String : AnyObject]) {
        print(userInfo)
        
        dictionaryWithInfo = userInfo
        print("recebeu dados do iphone \(userInfo)")
        
        if let presentationTotalTime = userInfo["presentationTotalTime"]
        {
            //coloca a variável em algum lugar
            print("tempo total da apresentacao: \(presentationTotalTime)")
        }
        if let notificationsTime = userInfo["notifications"]
        {
            //coloca a variável em algum lugar
            print("tempo dos haptics: \(notificationsTime)")
        }
        if let blockNames = userInfo["blockNames"]
        {
            //coloca a variável em algum lugar
            print("nome dos blocks \(blockNames)")
        }
        
        pushControllerWithName("playingPresentation", context: userInfo)
    }
    
    //--------------------------------
    // VARIABLES ---------------------
    
    var dictionaryWithInfo = [String:AnyObject]()
    
    
    //--------------------------------
    // OUTLETS
    @IBOutlet var sessionLabel: WKInterfaceLabel!
    @IBOutlet var bulletTimeImage: WKInterfaceImage!
    @IBOutlet var bulletSessionImage: WKInterfaceImage!
    @IBOutlet var timeLabel: WKInterfaceLabel!
    @IBOutlet var numberOfSessionLabel: WKInterfaceLabel!
    @IBOutlet var sessionListLabel: WKInterfaceLabel!
    
    var contextAsDictionary : Dictionary<String, AnyObject> = ["":""]
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        print(context)
        session = WCSession.defaultSession()
        
        
        
        let bulletImage = UIImage(named: "bullet")!
        if context != nil
        {
            if let blockName = context!["blockNames"] as? Array<String>
            {
                sessionListLabel.setText("\(blockName)")
                if let name = context!["name"] as? String
                {
                    sessionLabel.setText(name)
                    if let presentationTime = context!["presentationTotalTime"] as? NSTimeInterval
                    {
                        timeLabel.setText("\(Int(presentationTime))")
                        if let notification = context!["notifications"] as? Array<Int>
                        {
                            numberOfSessionLabel.setText("\(notification.count)")
                        
                            contextAsDictionary = ["name" : name, "presentationTotalTime" : presentationTime, "blockNames" : blockName, "notifications" : notification]
                        }
                    }
                }
            }
            
        }
        
        
        bulletTimeImage.setImage(bulletImage)
        bulletTimeImage.sizeToFitWidth()
        bulletTimeImage.sizeToFitHeight()
        
        bulletSessionImage.setImage(bulletImage
        )
        bulletSessionImage.sizeToFitWidth()
        bulletSessionImage.sizeToFitHeight()

    }

    @IBAction func StartPresentation() {
        pushControllerWithName("playingPresentation", context: contextAsDictionary)
    }
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    
    //--------------------------------
    // MARK: CUSTOM METHODS
  
}
