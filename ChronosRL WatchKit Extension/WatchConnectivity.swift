//
//  WatchConnectivity.swift
//  ChronosRL
//
//  Created by Felipe Lukavei Ferreira on 4/17/16.
//  Copyright © 2016 Felipe Lukavei Ferreira. All rights reserved.
//

import WatchKit
import WatchConnectivity
class WatchConnectivity: NSObject, WCSessionDelegate {

    var dataDictionary : Dictionary<String, AnyObject> = ["":""]
    
    
    var session : WCSession?{
        didSet{
            if let session = session{
                session.delegate = self
                session.activateSession()
            }
        }
    }

    func session(session: WCSession, didReceiveUserInfo userInfo: [String : AnyObject]) {
        
        if WCSession.isSupported() {
            let session = WCSession.defaultSession()
            session.activateSession()
        }
        
        dataDictionary = userInfo
    }
    

    
}
