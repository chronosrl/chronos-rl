//
//  InterfaceController.swift
//  ChronosRL WatchKit Extension
//
//  Created by Felipe Lukavei Ferreira on 4/6/16.
//  Copyright © 2016 Felipe Lukavei Ferreira. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

struct Presentation
{
    let name: String
    let totalTime: NSTimeInterval
    let notifications: Array<Int>
    let blockNames: Array<String>
}



class InterfaceController: WKInterfaceController, WCSessionDelegate {

    var presentations : [Presentation] = [/*Presentation(name: "NAME", totalTime: NSTimeInterval(10), notifications: [5,10], blockNames: ["meio", "fim"])*/]
    
    var session : WCSession?{
        didSet{
            if let session = session{
                session.delegate = self
                session.activateSession()
            }
        }
    }

    func session(session: WCSession, didReceiveMessage message: [String : AnyObject]) {
        let presentationsArray:Array<Dictionary<String,AnyObject>>?
        
        presentations.removeAll()
        presentationsArray = message["presentations"] as? Array<Dictionary<String,AnyObject>>
        
        if let _ = presentationsArray
        {
            for presentationDictionary in presentationsArray!
            {
                let presentation:Presentation
                
                let presentationName = presentationDictionary["name"] as! String
                let marksArray = presentationDictionary["notifications"] as! Array<Int>
                let blocksNames = presentationDictionary["blockNames"] as! Array<String>
                let presentationDuration = presentationDictionary["presentationTotalTime"] as! NSTimeInterval
                
                presentation = Presentation(name: presentationName, totalTime: presentationDuration, notifications: marksArray, blockNames: blocksNames)
                
                self.presentations.append(presentation)
                
            }
            createAndPopulateTable()
        }
        

    }
    func session(session: WCSession, didReceiveUserInfo userInfo: [String : AnyObject]) {
        print("recebeu dados do iphone")
        
        if presentations.count != 0
        {
            var presentationNames : [String] = []
            for present in presentations
            {
                presentationNames.append(present.name)
            }
            
            addPresentations(userInfo, presentationNames: presentationNames)
        }
        else
        {
            addPresentations(userInfo, presentationNames: nil)
        }
        
        pushControllerWithName("playingPresentation", context: userInfo)

    }
    
    
    func addPresentations (userInfo: [String: AnyObject], presentationNames: [String]?)
    {
        print("presentationNames: \(presentationNames); userinfo: \(userInfo)")
        if let info = userInfo["presentations"] as? Array<Dictionary<String, AnyObject>>
        {
            for present in info
            {
                if let totalTime = present["presentationTotalTime"] as? NSNumber
                {
                    if let notifications = present["notifications"] as? Array<Int>
                    {
                        if let blockNames = present["blockNames"] as? Array<String>
                        {
                            let presentationToAdd : Presentation = Presentation(name: "\(present["name"])", totalTime: NSTimeInterval(totalTime), notifications: notifications, blockNames: blockNames)
                            if let presentNames = presentationNames
                            {
                                if !presentNames.contains(presentationToAdd.name)
                                {
                                    presentations.append(presentationToAdd)
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    @IBOutlet var table: WKInterfaceTable!
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        // Configure interface objects here.
        
        session = WCSession.defaultSession()
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        
        session = WCSession.defaultSession()
        var presentationNames : [String] = []
        for presentation in presentations
        {
            presentationNames.append(presentation.name)
        }
        if presentationNames.count == 0
        {
            table.setNumberOfRows(1, withRowType: "noPresentationRow")
        }
        else
        {
            table.setNumberOfRows(presentationNames.count, withRowType: "row")
            for index in 0...presentationNames.count-1
            {
                if let row = table.rowControllerAtIndex(index) as? rowController
                {
                    row.label.setText(presentationNames[index])
                }
            }
        }
        super.willActivate()
    }
    
    
    func createAndPopulateTable()
    {
        if presentations.count == 0
        {
            table.setNumberOfRows(1, withRowType: "noPresentationRow")
        }
        else
        {
            table.setNumberOfRows(presentations.count, withRowType: "row")
            for index in 0...presentations.count-1
            {
                if let row = table.rowControllerAtIndex(index) as? rowController
                {
                    row.label.setText(presentations[index].name)
                }
            }
        }

    }

    override func table(table: WKInterfaceTable, didSelectRowAtIndex rowIndex: Int) {
        if presentations.count != 0
        {
            let presentationOfRow = presentations[rowIndex]
            let contextAsDictionary = ["name" : presentationOfRow.name, "presentationTotalTime" : presentationOfRow.totalTime, "blockNames" : presentationOfRow.blockNames, "notifications" : presentationOfRow.notifications]
            pushControllerWithName("presentationInfo", context: contextAsDictionary)
        }
    }
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
